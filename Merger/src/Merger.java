import java.util.LinkedList;
import java.util.List;

public class Merger {


    private List<LinkedList<String>> mainList;

    private String option;

    public Merger(List list, String option) {
        mainList = list;
        this.option = option;
    }

    public LinkedList<String> sortI(int n) {

        LinkedList<String> result = new LinkedList<>();

        int buffer;
        if (option.equals("-a")) {
            buffer = Integer.MAX_VALUE;
        } else {
            buffer = Integer.MIN_VALUE;
        }


        int targetI = -1;
        while (n != 0) {
            for (int i = 0; i < mainList.size(); i++) {
                if (option.equals("-a")) {
                    if (mainList.get(i).size() != 0 && buffer >= Integer.parseInt(mainList.get(i).getFirst())) {
                        buffer = Integer.parseInt(mainList.get(i).getFirst());
                        targetI = i;
                    }
                } else {
                    if (mainList.get(i).size() != 0 && buffer <= Integer.parseInt(mainList.get(i).getLast())) {
                        buffer = Integer.parseInt(mainList.get(i).getLast());
                        targetI = i;
                    }
                }
            }
            result.add(Integer.toString(buffer));



            if (option.equals("-a")) {
                if (mainList.get(targetI).size() != 0) {
                    mainList.get(targetI).removeFirst();
                }
                buffer = Integer.MAX_VALUE;
            } else {
                if (mainList.get(targetI).size() != 0) {
                    mainList.get(targetI).removeLast();
                }
                buffer = Integer.MIN_VALUE;
            }
            n--;
        }
        return result;
    }

    public LinkedList<String> sortS(int n) {

        LinkedList<String> result = new LinkedList<>();

        String buffer;
        if (option.equals("-a")) {
            char ch = (char)127;
            buffer = String.valueOf(ch);
        } else {
            char ch = (char)0;
            buffer = String.valueOf(ch);
        }


        int targetI = -1;
        while (n != 0) {
            for (int i = 0; i < mainList.size(); i++) {
                if (option.equals("-a")) {
                    if (mainList.get(i).size() != 0 && buffer.compareTo(mainList.get(i).getFirst()) >= 0) {
                        buffer = mainList.get(i).getFirst();
                        targetI = i;
                    }
                } else {
                    if (mainList.get(i).size() != 0 && buffer.compareTo(mainList.get(i).getLast()) <= 0) {
                        buffer = mainList.get(i).getLast();
                        targetI = i;
                    }
                }
            }
            result.add(buffer);

            if (option.equals("-a")) {
                if (mainList.get(targetI).size() != 0) {
                    mainList.get(targetI).removeFirst();
                }
                char ch = (char)65000;
                buffer = String.valueOf(ch);
            } else {
                if (mainList.get(targetI).size() != 0) {
                    mainList.get(targetI).removeLast();
                }
                char ch = (char)0;
                buffer = String.valueOf(ch);
            }
            n--;
        }
        return result;
    }

}
