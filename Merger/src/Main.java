import java.io.*;
import java.util.*;

public class Main {

    private static int n = 0;
    private static String dataType;

    public static void main(String[] args) {

        List<String> listInputFile = new ArrayList<>();
        String option = "-a", outFile;

        if (args.length < 3) {
            System.out.println("�� ����� �� ��� ��������!");
        }


        if (args[0].equals("-a") || args[0].equals("-d")) {
            option = args[0];
            dataType = args[1];
            outFile = args[2];

            for (int i = 3; i < args.length; i++) {
                listInputFile.add(args[i]);
            }
        } else {
            if (!args[0].equals("-s") && !args[0].equals("-i")) {
                System.out.println("�� ����� �� ������ ��������!");
            }
            dataType = args[0];
            outFile = args[1];
            for (int i = 2; i < args.length; i++) {
                listInputFile.add(args[i]);
            }
        }


        List<LinkedList<String>> mainList = new ArrayList<>();

        for (int i = 0; i < listInputFile.size(); i++) {
            mainList.add(readOnFile(listInputFile.get(i)));
        }

        for (int i = 0; i < mainList.size(); i++) {
            n += mainList.get(i).size();
        }


        Merger m = new Merger(mainList, option);
        LinkedList<String> r;
        if (dataType.equals("-i")) {
            r = m.sortI(n);
        } else {
            r = m.sortS(n);
        }

        writeOnFile(outFile, r);



    }

    private static LinkedList<String> readOnFile(String fileP) {
        LinkedList<String> list = new LinkedList<>();

        try {
            File file = new File(fileP);
            FileReader fr = new FileReader(file);
            BufferedReader reader = new BufferedReader(fr);

            String line = reader.readLine();

            while (line != null) {
                list.add(line);
                line = reader.readLine();
            }
            fr.close();
            reader.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return list;
    }

    private static void writeOnFile(String path, List<String> list) {
        FileWriter writer = null;
        try {
            writer = new FileWriter(path);
            for (String line : list) {
                writer.write(line + "\n");
            }
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
